package com.example.heath.photopicker

import android.content.ContentResolver
import android.provider.MediaStore

/**
 * Created by heath on 2017. 1. 20..
 */
object ImageUtils {

    /**
     * Return a path of a thumbnail image by an image id.
     * If an image is not exist, it'll be requested with MINI_KIND option.
     */
    fun getThumbnailPath(contentResolver: ContentResolver, imageId: Int, makeIfNotExist: Boolean): String? {
        var path: String? = null
        val thumbCursor = MediaStore.Images.Thumbnails.queryMiniThumbnail(contentResolver, imageId.toLong(), MediaStore.Images.Thumbnails.MINI_KIND, null)
        if (thumbCursor != null && thumbCursor.count > 0) {
            thumbCursor.moveToFirst()
            path = thumbCursor.getString(thumbCursor.getColumnIndexOrThrow(MediaStore.Images.Thumbnails.DATA))
            thumbCursor.close()
        } else {
            if (makeIfNotExist) {
                println("create thumbnail!")
                thumbCursor.close()
                val bitmap = MediaStore.Images.Thumbnails.getThumbnail(contentResolver, imageId.toLong(), MediaStore.Images.Thumbnails.MINI_KIND, null)
                if (bitmap != null) {
                    return getThumbnailPath(contentResolver, imageId, false)
                } else {
                    return null
                }
            }
        }

        return path
    }

}