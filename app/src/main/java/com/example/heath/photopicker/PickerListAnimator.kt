package com.example.heath.photopicker

import android.support.v7.widget.DefaultItemAnimator

/**
 * Created by heath on 2017. 1. 25..
 */
class PickerListAnimator : DefaultItemAnimator() {
    init {
        supportsChangeAnimations = false
    }
}