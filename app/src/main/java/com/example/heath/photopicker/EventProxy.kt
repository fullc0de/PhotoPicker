package com.example.heath.photopicker

import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject

/**
 * Created by heath on 2017. 1. 24..
 */
class EventProxy {
    val subject: PublishSubject<Event> = PublishSubject.create<Event>()

    fun send(position: Int, data: Any) {
        subject.onNext(Event(position, data))
    }

    fun send(position: Int, backgroundDataDispatcher: () -> Any?) {
        Single.fromCallable(backgroundDataDispatcher)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .filter { it != null }
                .subscribe { send(position, it!!) }
    }

    fun observable(): Observable<Event> {
        return subject.toSerialized()
    }
    class Event(val position: Int, val data: Any)
}