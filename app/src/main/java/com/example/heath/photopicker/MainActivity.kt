package com.example.heath.photopicker

import android.Manifest
import android.content.Context
import android.content.CursorLoader
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.item_cardview.view.*

import java.io.File

class MainActivity : AppCompatActivity(), android.app.LoaderManager.LoaderCallbacks<Cursor> {

    val SpanCount = 4
    val adapter = PhotoItemAdapter(this, null)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //picker_button.setOnClickListener { v -> showPickerIfHasPermission() }

        adapter.onItemClicks().subscribe { e ->
            println("click pos = ${e.position}, data = ${e.data as Int}")
        }

        val layoutManager = GridLayoutManager(this, SpanCount)
        recyclerview.layoutManager = layoutManager
        recyclerview.itemAnimator = null
        recyclerview.adapter = adapter
        recyclerview.setHasFixedSize(true)
        recyclerview.addItemDecoration(PickerDecoration(3, SpanCount))

        showPickerIfHasPermission()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            200 -> {
                if (grantResults.count() > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    showPicker()
                } else {
                    Log.d("debug", "a request is denied")
                }
            }
            else -> Log.d("debug", "unknown result")
        }
    }

    override fun onCreateLoader(id: Int, args: Bundle?): android.content.Loader<Cursor> {
        return CursorLoader(applicationContext,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                arrayOf(MediaStore.Images.Media._ID, MediaStore.Images.Media.DATA),
                null,
                null,
                null)
    }

    override fun onLoadFinished(loader: android.content.Loader<Cursor>?, data: Cursor?) {
        adapter.swapCursor(data)
        recyclerview.invalidate()
    }

    override fun onLoaderReset(loader: android.content.Loader<Cursor>?) {
        adapter.swapCursor(null)
    }

    private fun showPickerIfHasPermission() {
        val checkState = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
        if (checkState == PackageManager.PERMISSION_GRANTED) {
            showPicker()
        } else {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), 200)
        }
    }

    private fun showPicker() {
        loaderManager.initLoader<Cursor>(0, null, this)
    }

    class PhotoItemAdapter(context: Context, cursor: Cursor?) : CursorRecyclerViewAdapter<MainActivity.PhotoItemAdapter.ViewHolder>(context, cursor) {

        var lastPosition: Int = -1
        var selectedState = mutableListOf<Int>()

        val eventProxy = EventProxy()

        override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
            val itemView = LayoutInflater.from(parent!!.context).inflate(R.layout.item_cardview, parent, false)

            hasStableIds()
            val vh = ViewHolder(itemView)
            itemView.setOnClickListener { v ->
                val pos = vh.adapterPosition
                eventProxy.send(pos) {
                    cursor.getInt(cursor.getColumnIndex(MediaStore.Images.Media._ID))
                }
                switchSelectedState(pos)
                notifyItemChanged(pos)
                updateSelectedOrdinal()
            }

            return vh
        }

        override fun onBindViewHolder(viewHolder: ViewHolder?, cursor: Cursor?) {
            try {
                if (cursor != null && viewHolder != null) {

                    val imgId = cursor.getInt(cursor.getColumnIndex(MediaStore.Images.Media._ID))
                    viewHolder.showImageById(imgId)

                    val pos = viewHolder.adapterPosition
                    viewHolder.itemView.border_overlay.visibility = if (isSelected(pos)) View.VISIBLE else View.GONE
                    viewHolder.itemView.ordinal_textview.text = selectedState.indexOf(pos).toString()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        fun onItemClicks(): Observable<EventProxy.Event> {
            return eventProxy.observable()
        }

        private fun updateSelectedOrdinal() {
            selectedState.forEach { notifyItemChanged(it) }
        }

        private fun isSelected(position: Int): Boolean {
            return selectedState.contains(position)
        }

        private fun switchSelectedState(position: Int): Boolean {
            if (isSelected(position)) {
                selectedState.remove(position)
                return false
            } else {
                selectedState.add(position)
                return true
            }
        }

        class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var imageView: ImageView = view.image_view
            var lastImageId = -1

            fun showImageById(id: Int) {

                if (lastImageId == id) {
                    return
                }
                imageView.setImageDrawable(null)

                Single.fromCallable { ImageUtils.getThumbnailPath(itemView.context.contentResolver, id, true) }
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .filter { it != null }
                        .subscribe { path ->
                            Glide.with(itemView.context)
                                .load(File(path))
                                .override(200, 200)
                                .centerCrop()
                                .dontAnimate()
                                .into(imageView)
                            lastImageId = id
                        }
            }
        }
    }
}
