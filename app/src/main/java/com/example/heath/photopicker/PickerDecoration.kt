package com.example.heath.photopicker

import android.graphics.Canvas
import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View

/**
 * Created by heath on 2017. 1. 20..
 */
class PickerDecoration(val space: Int, val spanCount: Int) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect?, view: View?, parent: RecyclerView?, state: RecyclerView.State?) {
        if (spanCount == 0) {
            return
        }

        if (outRect != null) {
            outRect.left = space
            outRect.right = 0
            outRect.bottom = space
            outRect.top = space

            if (parent != null) {
                val pos = parent.getChildAdapterPosition(view)
                if (pos >= spanCount) {
                    outRect.top = 0
                }
            }
        }

        //println("item count = ${state?.itemCount}")
    }

    override fun onDrawOver(c: Canvas?, parent: RecyclerView?, state: RecyclerView.State?) {

    }
}